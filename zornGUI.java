import java.awt.*;
import javax.swing.*;

public class zornGUI extends JFrame {

  private JPanel mainPanel;
  private JPanel playfield;
  private JPanel stats;

  public zornGUI() {
    setTitle("Zorn");
    setSize(400, 300);
    setResizable(false);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    initComponents();
  }

  private void initComponents() {
    Container content = getContentPane();

    mainPanel = new JPanel();
    playfield = new JPanel();
    stats     = new JPanel();

    mainPanel.setLayout(new BorderLayout());
    mainPanel.add(playfield, BorderLayout.CENTER);
    mainPanel.add(stats, BorderLayout.EAST);

    setVisible(true);

  }

}